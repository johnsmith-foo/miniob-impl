/* Copyright (c) 2021 Xie Meiyi(xiemeiyi@hust.edu.cn) and OceanBase and/or its affiliates. All rights reserved.
miniob is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details. */

//
// Created by Wangyunlai on 2022/5/22.
//

#include "rc.h"
#include "common/log/log.h"
#include "common/lang/string.h"
#include "sql/stmt/filter_stmt.h"
#include "storage/common/db.h"
#include "storage/common/table.h"
#include "util/date.h"

FilterStmt::~FilterStmt()
{
  for (FilterUnit *unit : filter_units_) {
    delete unit;
  }
  filter_units_.clear();
}

RC FilterStmt::create(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
		      Condition *conditions, int condition_num,
		      FilterStmt *&stmt)
{
  RC rc = RC::SUCCESS;
  stmt = nullptr;

  FilterStmt *tmp_stmt = new FilterStmt();
  for (int i = 0; i < condition_num; i++) {
    FilterUnit *filter_unit = nullptr;
    rc = create_filter_unit(db, default_table, tables, conditions[i], filter_unit);
    if (rc != RC::SUCCESS) {
      delete tmp_stmt;
      LOG_WARN("failed to create filter unit. condition index=%d", i);
      return rc;
    }
    tmp_stmt->filter_units_.push_back(filter_unit);
  }

  stmt = tmp_stmt;
  return rc;
}

RC get_table_and_field(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
		       const RelAttr &attr, Table *&table, const FieldMeta *&field)
{
  if (common::is_blank(attr.relation_name)) {
    table = default_table;
  } else if (nullptr != tables) {
    auto iter = tables->find(std::string(attr.relation_name));
    if (iter != tables->end()) {
      table = iter->second;
    }
  } else {
    table = db->find_table(attr.relation_name);
  }
  if (nullptr == table) {
    LOG_WARN("No such table: attr.relation_name: %s", attr.relation_name);
    return RC::SCHEMA_TABLE_NOT_EXIST;
  }

  field = table->table_meta().field(attr.attribute_name);
  if (nullptr == field) {
    LOG_WARN("no such field in table: table %s, field %s", table->name(), attr.attribute_name);
    table = nullptr;
    return RC::SCHEMA_FIELD_NOT_EXIST;
  }

  return RC::SUCCESS;
}

RC FilterStmt::create_filter_unit(Db *db, Table *default_table, std::unordered_map<std::string, Table *> *tables,
				  Condition &condition, FilterUnit *&filter_unit)
{
  RC rc = RC::SUCCESS;
  
  CompOp comp = condition.comp;
  if (comp < EQUAL_TO || comp >= NO_OP) {
    LOG_WARN("invalid compare operator : %d", comp);
    return RC::INVALID_ARGUMENT;
  }

  Expression *left = nullptr;
  Expression *right = nullptr;
  if (condition.left_is_attr) {
    Table *table = nullptr;
    const FieldMeta *field = nullptr;
    rc = get_table_and_field(db, default_table, tables, condition.left_attr, table, field);  
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      return rc;
    }
    left = new FieldExpr(table, field);
  } else {
    if (condition.left_value.type == CHARS) {
      int32_t date = -1;
      const char *str = static_cast<const char*>(condition.left_value.data);
      int ret = string_to_date(str, &date);
      if (ret < 0) {
        LOG_TRACE("failed to parse string to date, s=%s", str);
      } else {
        value_destroy(&condition.left_value);
        value_init_date(&condition.left_value, date);
      }
    }
    left = new ValueExpr(condition.left_value);
  }

  if (condition.right_is_attr) {
    Table *table = nullptr;
    const FieldMeta *field = nullptr;
    rc = get_table_and_field(db, default_table, tables, condition.right_attr, table, field);  
    if (rc != RC::SUCCESS) {
      LOG_WARN("cannot find attr");
      delete left;
      return rc;
    }
    right = new FieldExpr(table, field);
  } else {
    if (condition.right_value.type == CHARS) {
      int32_t date = -1;
      const char *str = static_cast<const char*>(condition.right_value.data);
      int ret = string_to_date(str, &date);
      if (ret < 0) {
        LOG_TRACE("failed to parse string to date, s=%s", str);
      } else {
        value_destroy(&condition.right_value);
        value_init_date(&condition.right_value, date);
      }
    }
    right = new ValueExpr(condition.right_value);
  }

  filter_unit = new FilterUnit;
  filter_unit->set_comp(comp);
  filter_unit->set_left(left);
  filter_unit->set_right(right);

  // 检查两个类型是否能够比较
  if ((rc = is_expression_comparable(*filter_unit)) != RC::SUCCESS) {
    LOG_ERROR("left expr attr type differs from right expr attr type.");
  }
  return rc;
}

class ExpressionCompareAdapter {
public:
  ExpressionCompareAdapter() = delete;
  ExpressionCompareAdapter(Expression *expr) : expr_(expr)
  {}

public:
  inline AttrType attr_type()
  {
    AttrType type = AttrType::UNDEFINED;
    switch (expr_->type()) {
      case ExprType::FIELD: {
        auto field = static_cast<FieldExpr *>(expr_)->field();
        type = field.attr_type();
      } break;
      case ExprType::VALUE: {
        auto value_expr = static_cast<ValueExpr *>(expr_);
        TupleCell cell;
        value_expr->get_tuple_cell(cell);
        type = cell.attr_type();
      } break;
      default:
        LOG_WARN("not implemented.");
        break;
    }
    return type;
  }

private:
  Expression *expr_;
};

RC FilterStmt::is_expression_comparable(const FilterUnit &filter_unit) {
  RC rc = RC::SUCCESS;

  Expression *left = filter_unit.left();
  Expression *right = filter_unit.right();
  ExpressionCompareAdapter left_adapter(left);
  ExpressionCompareAdapter right_adapter(right);

  if (left_adapter.attr_type() != right_adapter.attr_type()) {
    rc = RC::SCHEMA_FIELD_TYPE_MISMATCH;
  }
  return rc;
}